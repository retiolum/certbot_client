<?php

declare(strict_types=1);

// Set default timezone.
date_default_timezone_set('UTC');

// Disable time limit.
set_time_limit(0);

// Define the base path of the application.
define('APP_BASE_PATH', Phar::running() ? Phar::running() : dirname(__FILE__));
define('WORKING_DIRECTORY', Phar::running(false) ? dirname(Phar::running(false)) : dirname(__FILE__));

// Include Composer's autoloader.
$loader = require_once APP_BASE_PATH . '/vendor/autoload.php';

// Use and start the application.
$app = new \retiolum\CertbotClient\Application('Certbot client', '@VERSION@');
$app->run();
