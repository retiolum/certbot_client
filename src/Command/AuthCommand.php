<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for the auth hook (i.e. for setting up the validation code).
 */
class AuthCommand extends AbstractWorkCommand
{
    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        parent::configure();

        $this->setName('auth')
            ->setDescription('Handle the authentication step (i.e. upload the auth file to the server)');
    }

    /**
     * @inheritdoc
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->title($this->getName());

        // Create the required directory.
        if (!$this->remote->has(self::CERTIFICATION_DIR)) {
            $this->remote->createDirectory(self::CERTIFICATION_DIR);
        }

        // Place the auth file.
        $this->remote->write(
            self::CERTIFICATION_DIR . DIRECTORY_SEPARATOR . $this->environment['CERTBOT_TOKEN'],
            $this->environment['CERTBOT_VALIDATION']
        );

        $this->io->success('Auth file written to ' . self::CERTIFICATION_DIR . DIRECTORY_SEPARATOR . $this->environment['CERTBOT_TOKEN']);

        return self::SUCCESS;
    }
}
