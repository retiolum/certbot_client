<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Command;

use Spatie\SslCertificate\SslCertificate;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use retiolum\CertbotClient\Configuration\ConfigurationTrait;

/**
 * Command for checking the certificates of the configured servers.
 */
class CheckCommand extends AbstractCommand
{
    use ConfigurationTrait;

    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        parent::configure();

        $this->setName('check')
            ->setDescription('Check certificates of configured servers');
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);

        // Read configuration file.
        $this->loadConfiguration();
    }

    /**
     * @inheritdoc
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->title($this->getName());

        // Check if configuration file exists.
        if (!is_file($this->configurationFile)) {
            throw new \RuntimeException(sprintf('Configuration file %s does not exist.', $this->configurationFile));
        }

        // Output information about the single domains.
        foreach ($this->configuration['domains'] as $domain => $settings) {
            $this->checkCertificateForDomain($domain);
        }

        return self::SUCCESS;
    }

    /**
     * Check a domain's certificate.
     *
     * @param string $domain Domain to check
     */
    protected function checkCertificateForDomain(string $domain): void
    {
        $this->io->info('Domain: ' . $domain);
        try {
            $certificate = SslCertificate::createForHostName($domain);
            $this->io->horizontalTable(
                [
                    'Issuer',
                    'Valid from',
                    'Valid to',
                    'Additional domains',
                ],
                [
                    [
                        $certificate->getIssuer(),
                        $certificate->validFromDate()->toIso8601String(),
                        $certificate->expirationDate()->toIso8601String(),
                        implode(
                            ', ',
                            array_filter(
                                $certificate->getAdditionalDomains(),
                                static fn(string $item): bool => $item !== $domain
                            )
                        ),
                    ],
                ]
            );
        } catch (\Throwable $error) {
            $this->io->error('Certificate could not be retrieved: ' . $error->getMessage());
        }
    }
}
