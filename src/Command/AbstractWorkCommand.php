<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Command;

use League\Flysystem\Filesystem;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use retiolum\CertbotClient\Configuration\ConfigurationTrait;

/**
 * Abstract base class for all really working commands.
 */
abstract class AbstractWorkCommand extends AbstractCommand
{
    use ConfigurationTrait;

    public const CERTIFICATION_DIR = '.well-known/acme-challenge'; // Directory where certification files should be placed

    /**
     * Names of environment variables handed over from Certbot
     *
     * @var array
     */
    protected array $environmentNames = [
        'CERTBOT_DOMAIN',
        'CERTBOT_VALIDATION',
        'CERTBOT_TOKEN',
    ];

    /**
     * Environment variables handed over from Certbot
     *
     * @var array
     */
    protected array $environment;

    /**
     * Filesystem connected to remote server for doing the work
     *
     * @var Filesystem
     */
    protected Filesystem $remote;

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        parent::initialize($input, $output);

        // Read configuration file.
        $this->loadConfiguration();

        // Read environment variables.
        $this->readEnvironment();

        // Get the domain entry for the currently handled domain.
        $domain = $this->getDomain($this->environment['CERTBOT_DOMAIN']);
        if ($domain === false) {
            throw new \RuntimeException(
                sprintf(
                    'Domain not found in configuration (%s): %s',
                    $this->configurationFile,
                    $this->environment['CERTBOT_DOMAIN']
                )
            );
        }

        // Get the server for the currently handled domain.
        $server = $this->getServer($domain['server']);
        if ($server === false) {
            throw new \RuntimeException(
                sprintf('Server not found in configuration (%s): %s', $this->configurationFile, $domain['server'])
            );
        }

        // Connect to remote server.
        $this->connect($server, $domain['path']);
    }

    /**
     * Read environment variables handed over from Certbot.
     */
    protected function readEnvironment(): void
    {
        foreach ($this->environmentNames as $name) {
            if (empty($_SERVER[$name])) {
                throw new \RuntimeException(sprintf('Missing environment variable: %s', $name));
            }

            $this->environment[$name] = $_SERVER[$name];
        }
    }

    /**
     * Connect to the remote server (i.e. set up an instance of \League\Flysystem\Filesystem).
     *
     * @param array $server Server configuration
     * @param string $documentRoot Document root at server to connect to
     */
    protected function connect(array $server, string $documentRoot): void
    {
        // Prepare configuration for the adapter.
        $configuration = $server;
        unset($configuration['type']);
        $configuration['root'] = $documentRoot;

        // Prepare the adapter.
        switch ($server['type']) {
            case 'ftp':
                $options = new \League\Flysystem\Ftp\FtpConnectionOptions(
                    $configuration['host'],
                    $configuration['root'],
                    $configuration['username'],
                    $configuration['password'],
                    $configuration['port'] ?? 21
                );
                $adapter = new \League\Flysystem\Ftp\FtpAdapter($options);
                break;

            case 'sftp':
                $provider = new \League\Flysystem\PhpseclibV3\SftpConnectionProvider(
                    $configuration['host'],
                    $configuration['username'],
                    $configuration['password'],
                    null,
                    null,
                    $configuration['port'] ?? 22
                );
                $adapter = new \League\Flysystem\PhpseclibV3\SftpAdapter($provider, $configuration['root']);
                break;

            default:
                throw new \RuntimeException(sprintf('Unsupported connection type: %s', $server['type']));
        }

        // Prepare the filesystem.
        $this->remote = new Filesystem($adapter);
    }
}
