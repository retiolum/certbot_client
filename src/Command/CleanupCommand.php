<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for the cleanup hook called at the end of the process.
 */
class CleanupCommand extends AbstractWorkCommand
{
    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        parent::configure();

        $this->setName('cleanup')
            ->setDescription('Handle the cleanup step (i.e. remove the auth file from the server)');
    }

    /**
     * @inheritdoc
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->title($this->getName());

        // Remove the auth directory.
        $this->remote->deleteDirectory(dirname(self::CERTIFICATION_DIR));

        $this->io->success('Auth directory ' . self::CERTIFICATION_DIR . ' removed');

        return self::SUCCESS;
    }
}
