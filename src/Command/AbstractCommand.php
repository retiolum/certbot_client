<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Abstract base class for all commands.
 */
abstract class AbstractCommand extends Command
{
    /**
     * Path to configuration file
     *
     * @var string|null
     */
    protected ?string $configurationFile = null;

    /**
     * Symfony console style
     *
     * @var SymfonyStyle
     */
    protected SymfonyStyle $io;

    /**
     * Configure the basics for all commands.
     */
    protected function configure(): void
    {
        // Add option to specify configuration file.
        $this->addOption(
            'configuration',
            'c',
            InputOption::VALUE_REQUIRED,
            'Configuration file to use',
            'config.yml'
        );
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        // Handle configuration file path.
        $file = $input->getOption('configuration');
        $this->configurationFile = realpath($file) ?: WORKING_DIRECTORY . DIRECTORY_SEPARATOR . $file;

        // Create Symfony console style.
        $this->io = new SymfonyStyle($input, $output);
    }
}
