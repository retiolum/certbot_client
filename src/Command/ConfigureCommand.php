<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Command;

use Symfony\Component\Config\Definition\Dumper\YamlReferenceDumper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use retiolum\CertbotClient\Configuration\ServerConfiguration;

/**
 * Command for creating a new configuration.
 */
class ConfigureCommand extends AbstractCommand
{
    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        parent::configure();

        $this->setName('configure')
            ->setDescription('Create a new configuration file');
    }

    /**
     * @inheritdoc
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->title($this->getName());

        $dumper = new YamlReferenceDumper();
        $configuration = new ServerConfiguration();
        $sample = $dumper->dump($configuration);

        if (is_file($this->configurationFile)) {
            /** @var \Symfony\Component\Console\Helper\QuestionHelper $question */
            $question = $this->getHelper('question');
            $confirmation = $question->ask(
                $input,
                $output,
                new \Symfony\Component\Console\Question\ConfirmationQuestion(
                    'File ' . $this->configurationFile . ' exists, overwrite (y/n)? '
                )
            );
            if (!$confirmation) {
                return self::FAILURE;
            }
        }

        file_put_contents($this->configurationFile, $sample);

        $this->io->success('Sample configuration written to file ' . $this->configurationFile);

        return self::SUCCESS;
    }
}
