<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Command;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use retiolum\CertbotClient\Configuration\ServerConfiguration;

/**
 * Command for showing the configuration.
 */
class InfoCommand extends AbstractCommand
{
    /**
     * Parsed configuration
     *
     * @var array
     */
    protected array $configuration;

    /**
     * Configure the command.
     */
    protected function configure(): void
    {
        parent::configure();

        $this->setName('info')
            ->setDescription('Show configuration');
    }

    /**
     * @inheritdoc
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->title($this->getName());

        // Check if configuration file exists.
        if (!is_file($this->configurationFile)) {
            throw new \RuntimeException(sprintf('Configuration file %s does not exist.', $this->configurationFile));
        }

        // Load the file and normalize it.
        $config = Yaml::parse(file_get_contents($this->configurationFile));
        if ($config['settings']) {
            $config = $config['settings'];
        }

        // Process them with our configuration template.
        $processor = new Processor();
        $configuration = new ServerConfiguration();
        $this->configuration = $processor->processConfiguration(
            $configuration,
            [$config]
        );

        // Print configuration.
        $this->io->info(json_encode($this->configuration, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT));

        return self::SUCCESS;
    }
}
