<?php

declare(strict_types=1);

namespace retiolum\CertbotClient;

/**
 * Application class for the Certbot client.
 */
class Application extends \Symfony\Component\Console\Application
{
    /**
     * @inheritdoc
     */
    protected function getDefaultCommands(): array
    {
        // Get base commands from the parent class.
        $baseCommands = parent::getDefaultCommands();

        // Add our own commands.
        $baseCommands[] = new Command\AuthCommand();
        $baseCommands[] = new Command\CleanupCommand();
        $baseCommands[] = new Command\ConfigureCommand();
        $baseCommands[] = new Command\InfoCommand();
        $baseCommands[] = new Command\CheckCommand();

        return $baseCommands;
    }
}
