<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Configuration;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Config\Definition\Processor;

/**
 * Trait for handling the configuration.
 */
trait ConfigurationTrait
{
    /**
     * Parsed configuration
     *
     * @var array
     */
    protected array $configuration;

    /**
     * Load and parse configuration for a server.
     */
    protected function loadConfiguration(): void
    {
        // Check if configuration file exists.
        if (!is_file($this->configurationFile)) {
            throw new \RuntimeException(sprintf('Configuration file %s does not exist.', $this->configurationFile));
        }

        // Load the file and normalize it.
        $config = Yaml::parse(file_get_contents($this->configurationFile));
        if ($config['settings']) {
            $config = $config['settings'];
        }

        // Process them with our configuration template.
        $processor = new Processor();
        $configuration = new ServerConfiguration();
        $this->configuration = $processor->processConfiguration(
            $configuration,
            [$config]
        );
    }

    /**
     * Get the domain entry for a given domain name.
     *
     * @param string $domainName Domain name to get the domain entry for
     * @return bool|array
     */
    protected function getDomain(string $domainName): bool|array
    {
        $domainName = strtolower($domainName);
        foreach ($this->configuration['domains'] as $domain => $settings) {
            if ($domain === $domainName || in_array($domainName, $settings['alias'], true)) {
                return $settings;
            }
        }

        return false;
    }

    /**
     * Get the server entry for a given server name.
     *
     * @param string $serverName Server name to get the server for
     * @return bool|array
     */
    protected function getServer(string $serverName): bool|array
    {
        foreach ($this->configuration['servers'] as $server => $settings) {
            if ($server === $serverName) {
                return $settings;
            }
        }

        return false;
    }
}
