<?php

declare(strict_types=1);

namespace retiolum\CertbotClient\Configuration;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Definition of the server configuration.
 */
class ServerConfiguration implements ConfigurationInterface
{
    /**
     * @inheritdoc
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('settings');
        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->children()
            ->arrayNode('servers')
            ->useAttributeAsKey('name')
            ->prototype('array')
            ->children()
            ->enumNode('type')
            ->values(['ftp', 'sftp'])
            ->end()
            ->scalarNode('host')
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('username')
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('password')
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->integerNode('port')
            ->min(21)
            ->end()
            ->end()
            ->end()
            ->end()
            ->arrayNode('domains')
            ->useAttributeAsKey('name')
            ->prototype('array')
            ->children()
            ->arrayNode('alias')
            ->prototype('scalar')->end()
            ->end()
            ->scalarNode('path')
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->scalarNode('server')
            ->isRequired()
            ->cannotBeEmpty()
            ->end()
            ->end()
            ->end()
            ->end();

        return $treeBuilder;
    }
}
